package com.silvioapps.fragmentssample;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class MyFragment extends Fragment {
    public static final String KEY_FRAGMENT = "KEY_FRAGMENT";
    public static final String TAG_FRAGMENT = "TAG_FRAGMENT";
    private TextView nameTextView = null;
    private MyObject myObject = null;

    public static MyFragment newInstance(Serializable serializable){
        Log.i("TAG","MyFragment newInstance");

        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_FRAGMENT,serializable);

        MyFragment myFragment = new MyFragment();
        myFragment.setArguments(bundle);

        return myFragment;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        Log.i("TAG","MyFragment onAttach");
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        Log.i("TAG","MyFragment onCreate");

        myObject = (MyObject)getArguments().getSerializable(KEY_FRAGMENT);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        Log.i("TAG","MyFragment onCreateView");

        View view = layoutInflater.inflate(R.layout.layout_fragment,viewGroup,false);

        nameTextView = (TextView)view.findViewById(R.id.nameTextView);

        if(myObject != null){
            String name = myObject.getName();

            nameTextView.setText(name);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle){
        super.onActivityCreated(bundle);

        Log.i("TAG","MyFragment onActivityCreated");
    }

    @Override
    public void onStart(){
        super.onStart();

        Log.i("TAG","MyFragment onStart");
    }

    @Override
    public void onResume(){
        super.onResume();

        Log.i("TAG","MyFragment onResume");
    }

    @Override
    public void onPause(){
        super.onPause();

        Log.i("TAG","MyFragment onPause");
    }

    @Override
    public void onStop(){
        super.onStop();

        Log.i("TAG","MyFragment onStop");
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        Log.i("TAG","MyFragment onDestroyView");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        Log.i("TAG","MyFragment onDestroy");
    }

    @Override
    public void onDetach(){
        super.onDetach();

        Log.i("TAG","MyFragment onDetach");
    }
}
