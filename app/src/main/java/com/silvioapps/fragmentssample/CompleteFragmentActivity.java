package com.silvioapps.fragmentssample;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class CompleteFragmentActivity extends AppCompatActivity implements MyListFragment.IOnClickList {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_fragment);

        Log.i("TAG","CompleteFragmentActivity onCreate");
    }

    public void onClick(Object object){
        Log.i("TAG","CompleteFragmentActivity onClickList");

        if(isTablet()) {
            Log.i("TAG","CompleteFragmentActivity isTablet");

            MyFragment myFragment = MyFragment.newInstance((MyObject)object);

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.myFragment, myFragment,MyFragment.TAG_FRAGMENT);
            fragmentTransaction.commit();
        }
        else{
            Log.i("TAG","CompleteFragmentActivity NOT isTablet");

            Intent intent = new Intent(this, MyFragmentActivity.class);
            intent.putExtra(MyFragment.KEY_FRAGMENT, (MyObject) object);
            startActivity(intent);
        }
    }

    public boolean isTablet(){
        if(findViewById(R.id.tablet) != null){
            return true;
        }

        return false;
    }
}
