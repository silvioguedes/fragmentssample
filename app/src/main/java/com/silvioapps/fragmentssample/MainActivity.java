package com.silvioapps.fragmentssample;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button fragment = null;
    private Button listFragment = null;
    private Button completeFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment = (Button)findViewById(R.id.fragmentButton);
        fragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MyFragmentActivity.class);
                startActivity(intent);
            }
        });

        listFragment = (Button)findViewById(R.id.listFragmentButton);
        listFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MyListFragmentActivity.class);
                startActivity(intent);
            }
        });

        completeFragment = (Button)findViewById(R.id.completeFragmentButton);
        completeFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CompleteFragmentActivity.class);
                startActivity(intent);
            }
        });
    }
}
