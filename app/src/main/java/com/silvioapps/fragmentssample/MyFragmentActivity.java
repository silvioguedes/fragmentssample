package com.silvioapps.fragmentssample;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MyFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        Log.i("TAG","MyFragmentActivity onCreate");

        Intent intent = getIntent();
        MyObject myObject = (MyObject)intent.getSerializableExtra(MyFragment.KEY_FRAGMENT);

        MyFragment myFragment = MyFragment.newInstance(myObject);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragment, myFragment,MyFragment.TAG_FRAGMENT);
        fragmentTransaction.commit();
    }
}
