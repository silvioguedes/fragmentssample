package com.silvioapps.fragmentssample;

import android.graphics.Color;

import java.io.Serializable;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class MyObject implements Serializable {
    private String name;

    public MyObject(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
